SELECT S1.Name AS Student_Name
FROM
    Students AS S1
    JOIN Packages AS P1 ON S1.ID = P1.ID
    JOIN Friends AS F ON F.ID = S1.ID
    -- JOIN Students AS S2 ON S2.ID = F.Friend_ID -- to trace the names of the students friends
    JOIN Packages AS P2 ON P2.ID = F.Friend_ID
WHERE
    P2.Salary > P1.Salary
ORDER BY P2.Salary;